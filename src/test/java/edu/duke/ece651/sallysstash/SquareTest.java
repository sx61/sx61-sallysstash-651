package edu.duke.ece651.sallysstash;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class SquareTest {
  @Test
  public void test_Square() {
    
    Square s1 = new Square(0,0,"G");
    s1.setState("*");
    assertEquals("*", s1.getState());
    s1.setkeep(true);
    assertEquals(true, s1.isKeep());
    
    assertEquals(false, s1.isShown());
    assertEquals(false, s1.isMine());
    assertEquals(0, s1.getX());
    assertEquals("G",s1.getColor());
    assertEquals(0, s1.getY());
    s1.setX(1);
    s1.setY(1);
    assertEquals(1,s1.getX());
    assertEquals(1,s1.getY());
    Stack stack = new Stack("R",3, "normal");
    s1.setOwner(stack);
    s1.setMine(true);
    s1.setShown(true);
    s1.setColor("R");
    assertEquals("R",s1.getColor());
    assertEquals(stack, s1.getOwner());
  }
}
