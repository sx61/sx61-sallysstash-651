package edu.duke.ece651.sallysstash;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.*;

public class BoardTest {
  @Test
  public void test_Board() {
    Board b = new Board(10, 20);
    Stack s1 = new Stack("R", 3, "normal");
    Stack s2 = new Stack("R", 1, "SuperStack");
    Stack s3 = new Stack("R", 3, "SuperStack");
    Stack s4 = new Stack("R", 1, "CrazyStack");
    Stack s5 = new Stack("R", 3, "CrazyStack");

    ArrayList<Stack>crazy_stacks = new ArrayList<Stack>();
    crazy_stacks.add(s4);
    crazy_stacks.add(s5);
    ArrayList<Stack> stacks = new ArrayList<Stack>();
    stacks.add(s2);
    stacks.add(s3);
    
    assertEquals(true, b.valid_put_simple(s1, 3,4,"V"));
    b.put_simple_stack(s1, 3, 4, "V");
    ArrayList<Stack> normal_stack = new ArrayList<Stack>();
    normal_stack.add(s1);

    
    assertEquals(false, b.valid_put_simple(s1, 20,4,"V"));
    assertEquals(true, b.valid_put_simple(s1, 4,4,"H"));
    b.put_simple_stack(s1, 4, 4, "H");
    assertEquals(false, b.valid_put_simple(s1, 20, 4, "H"));

    assertEquals(true, b.valid_put_simple(s1, 7,4,"A"));
    b.put_simple_stack(s1, 7, 4, "A");
    assertEquals(false, b.valid_put_simple(s1, 20, 4, "A"));
    

    assertEquals(false, b.valid_put_simple(s1, 6,4,"O"));
    b.put_simple_stack(s1, 6, 4, "O");
    assertEquals(false, b.valid_put_simple(s1, 20, 4, "O"));

    ArrayList<Square> squares = s1.getStack();
    Square sq = squares.get(0);
    sq.setMine(true);

    assertEquals(false, b.valid_put_simple(s1, 6, 4, "O"));
    
    assertEquals(true, b.validputStack(stacks, 4, 10, "U"));
    assertEquals(true, b.validputStack(stacks, 4, 10, "D"));
    assertEquals(true, b.validputStack(stacks, 4, 10, "R"));
    assertEquals(true, b.validputStack(stacks, 4, 10, "L"));

    
    
    assertEquals(true, b.validputStack(crazy_stacks, 4, 10, "U"));
    assertEquals(true, b.validputStack(crazy_stacks, 4, 10, "D"));
    assertEquals(true, b.validputStack(crazy_stacks, 4, 10, "R"));
    assertEquals(true, b.validputStack(crazy_stacks, 4, 10, "L"));
    b.putStack(crazy_stacks, 4, 10, "U");

    
    assertEquals(false, b.validputStack(stacks,20,10,"U"));
    
    
    
    Square square = b.getSquare(1, 2);
    b.setSquare(3, 11);
    
    b.check(3, 0);
    b.check(7, 5);
    
    b.check(0, 3);
    assertEquals(false, b.isSolved());
    for (int i = 0; i < 10; i++) {
      for (int j = 0; j < 20; j++) {
        b.setSquare(i, j);
      }
    }
    assertEquals(true, b.isSolved());
    


    Stack find_stack = b.find_stack_by_cell(3, 4);
    ArrayList<Square> copy_squares = b.copy_stack_from_board(crazy_stacks);
    b.remove_test(crazy_stacks);
    b.restore_stack(copy_squares);
    b.remove_stack(crazy_stacks);
    
    
    }

}
