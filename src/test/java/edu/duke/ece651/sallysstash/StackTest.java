package edu.duke.ece651.sallysstash;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import java.util.*;
  
public class StackTest {
  @Test
  public void test_Stack() {
    Stack stack = new Stack("R",3, "normal");
    assertEquals("normal", stack.getShape());
    assertEquals("R", stack.getStackColor());
    ArrayList<Square> Squares = stack.getStack();
    assertEquals("R", stack.getStackColor());
    assertEquals(3,stack.getSize());
    // assertEquals(5, stack.getSize());
  }

}
